package com.thbent.alarm.exception;

public class AlarmException extends Exception {

	private static final long serialVersionUID = -2646770638022412982L;

	public AlarmException() {
		super();
	}

	public AlarmException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public AlarmException(String message, Throwable cause) {
		super(message, cause);
	}

	public AlarmException(String message) {
		super(message);
	}

	public AlarmException(Throwable cause) {
		super(cause);
	}

}
