package com.thbent.alarm.exception;

/**
 * 登录异常
 * 
 * @author Administrator
 *
 */
public class LoginException extends Exception {

	private static final long serialVersionUID = -2826041720854220711L;

	private Integer errorCount;

	public LoginException() {
		super();
	}

	public LoginException(String message, Integer errorCount) {
		super(message);
		this.errorCount = errorCount;
	}

	public LoginException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public LoginException(String message, Throwable cause) {
		super(message, cause);
	}

	public LoginException(String message) {
		super(message);
	}

	public LoginException(Throwable cause) {
		super(cause);
	}

	public Integer getErrorCount() {
		return errorCount;
	}

	public void setErrorCount(Integer errorCount) {
		this.errorCount = errorCount;
	}

}
