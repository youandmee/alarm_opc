package com.thbent.alarm.dao;

import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.thbent.alarm.model.User;

public interface UserDao {

	User findByUserName(String username);

	void modifyErrorCount(Map<String, Object> paramMap);

	void modifyPassword(Map<String, Object> paramMap);

	void changePassword(@Param("username") String username,
			@Param("newPassword") String newPassword);

}
