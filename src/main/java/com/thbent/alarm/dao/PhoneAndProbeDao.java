package com.thbent.alarm.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.thbent.alarm.model.PhoneAndProbe;

public interface PhoneAndProbeDao {

	void add(PhoneAndProbe phoneAndProbe);

	List<PhoneAndProbe> findAll();

	List<PhoneAndProbe> findByPhoneId(@Param("phoneId") String phoneId);

	void deleteByPhoneId(String phoneId);

}
