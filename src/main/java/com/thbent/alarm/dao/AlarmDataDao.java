package com.thbent.alarm.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.thbent.alarm.model.AlarmData;

public interface AlarmDataDao {

	List<AlarmData> findAll();

	AlarmData findAlarmDataById(@Param("id") String id);

	void updateAlarmData(AlarmData alarmData);

}
