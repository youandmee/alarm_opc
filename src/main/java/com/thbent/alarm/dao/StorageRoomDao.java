package com.thbent.alarm.dao;

import java.util.List;

import com.thbent.alarm.model.StorageRoom;

public interface StorageRoomDao {

	List<StorageRoom> getAll();

}
