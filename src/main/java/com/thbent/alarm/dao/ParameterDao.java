package com.thbent.alarm.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.thbent.alarm.model.Parameter;

public interface ParameterDao {

	void add(Parameter parameter);

	Parameter find(@Param("name") String name);

	List<Parameter> findAllParameters();

	List<Parameter> findParameterByName(@Param("name") String name);

	Parameter findSingleParameter(@Param("name") String name);

	List<Parameter> findParametersByName(@Param("name") String name);

	void deleteById(@Param("phoneId") String id);

	Parameter findById(@Param("id") String id);

	void updatePhone(Parameter parameter);

	List<Parameter> findParameterByNameZh(@Param("nameZh") String nameZh);

	List<Parameter> findAllNameZh();

}
