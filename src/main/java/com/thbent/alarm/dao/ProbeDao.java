package com.thbent.alarm.dao;

import java.util.List;

import com.thbent.alarm.model.Probe;

public interface ProbeDao {

	List<Probe> findAll();

	List<Probe> findByStroraeRoomId(String storageRoomId);

}
