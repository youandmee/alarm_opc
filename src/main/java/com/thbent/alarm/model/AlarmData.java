package com.thbent.alarm.model;

public class AlarmData {

	/** 报警参数标识 */
	private String id;
	/** 值上限 */
	private double maxValue;
	/** 值下限 */
	private double minValue;
	/** 设备名称 */
	private String probeName;
	/** 仓库标识 */
	private String StorageRoomId;
	/** 仓库名字 */
	private String storageRoomName;

	public String getStorageRoomName() {
		return storageRoomName;
	}

	public void setStorageRoomName(String storageRoomName) {
		this.storageRoomName = storageRoomName;
	}

	public String getStorageRoomId() {
		return StorageRoomId;
	}

	public void setStorageRoomId(String storageRoomId) {
		StorageRoomId = storageRoomId;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public double getMaxValue() {
		return maxValue;
	}

	public void setMaxValue(double maxValue) {
		this.maxValue = maxValue;
	}

	public double getMinValue() {
		return minValue;
	}

	public void setMinValue(double minValue) {
		this.minValue = minValue;
	}

	public String getProbeName() {
		return probeName;
	}

	public void setProbeName(String probeName) {
		this.probeName = probeName;
	}

}
