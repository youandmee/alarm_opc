package com.thbent.alarm.model;

public class StorageRoom {

	/** 标识 */
	private String id;
	/** 库房名称 */
	private String storageRoomName;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getStorageRoomName() {
		return storageRoomName;
	}

	public void setStorageRoomName(String storageRoomName) {
		this.storageRoomName = storageRoomName;
	}

}
