package com.thbent.alarm.model;

/**
 * 手机号和设备的关系实体类
 * 
 * @author Administrator
 *
 */
public class PhoneAndProbe {

	/** 标识 */
	private String id;
	/** 手机号标识 */
	private String phoneId;
	/** 设备名称 */
	private String probeName;
	/** 仓库标识 */
	private String storageRoomId;
	/** 仓库名称 */
	private String storageRoomName;

	public String getStorageRoomName() {
		return storageRoomName;
	}

	public void setStorageRoomName(String storageRoomName) {
		this.storageRoomName = storageRoomName;
	}

	public String getStorageRoomId() {
		return storageRoomId;
	}

	public void setStorageRoomId(String storageRoomId) {
		this.storageRoomId = storageRoomId;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getPhoneId() {
		return phoneId;
	}

	public void setPhoneId(String phoneId) {
		this.phoneId = phoneId;
	}

	public String getProbeName() {
		return probeName;
	}

	public void setProbeName(String probeName) {
		this.probeName = probeName;
	}

}
