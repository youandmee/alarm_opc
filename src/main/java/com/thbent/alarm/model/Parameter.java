package com.thbent.alarm.model;

public class Parameter {

	/** 参数标识 */
	private String id;
	/** 参数名称 */
	private String name;
	/** 参数中文名称 */
	private String nameZh;
	/** 所属模块 */
	private String module;
	/** 值 */
	private String value;
	/** 查询是否是单一记录，或者集合 */
	private Integer isMultiple;
	/** 备注 */
	private String notes;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNameZh() {
		return nameZh;
	}

	public void setNameZh(String nameZh) {
		this.nameZh = nameZh;
	}

	public String getModule() {
		return module;
	}

	public void setModule(String module) {
		this.module = module;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public Integer getIsMultiple() {
		return isMultiple;
	}

	public void setIsMultiple(Integer isMultiple) {
		this.isMultiple = isMultiple;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

}
