package com.thbent.alarm.model;

/**
 * 用户表
 * 
 * @author Administrator
 *
 */
public class User {

	/** 用户标识 */
	private String id;
	/** 用户名 */
	private String username;
	/** 密码 */
	private String password;
	/** 错误次数，错误次数达到5次就重置密码 */
	private Integer errorCount;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Integer getErrorCount() {
		return errorCount;
	}

	public void setErrorCount(Integer errorCount) {
		this.errorCount = errorCount;
	}

}
