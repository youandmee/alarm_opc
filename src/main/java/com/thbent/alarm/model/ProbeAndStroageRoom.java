package com.thbent.alarm.model;

public class ProbeAndStroageRoom {

	/** 设备名称 */
	private String probeName;
	/** 仓库标识 */
	private String StorageroomId;
	/** 在仓库中使用的名字 */
	private String NameInStorageroom;

	public String getProbeName() {
		return probeName;
	}

	public void setProbeName(String probeName) {
		this.probeName = probeName;
	}

	public String getStorageroomId() {
		return StorageroomId;
	}

	public void setStorageroomId(String storageroomId) {
		StorageroomId = storageroomId;
	}

	public String getNameInStorageroom() {
		return NameInStorageroom;
	}

	public void setNameInStorageroom(String nameInStorageroom) {
		NameInStorageroom = nameInStorageroom;
	}

}
