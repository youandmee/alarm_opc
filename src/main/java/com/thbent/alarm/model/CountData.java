package com.thbent.alarm.model;

public class CountData {
	/** 名字 */
	private String name;
	/** 不正常次数 */
	private int count;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

}
