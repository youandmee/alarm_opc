package com.thbent.alarm.model;

import java.util.Date;

public class Probe {

	/** 读取时间 */
	private Date time;
	/** 设备名称 */
	private String tagName;
	/** 温度值 */
	private double pv;
	/** 数据不正常的次数 */
	private int count;
	/** 库标识 */
	private String storageRoomId;
	/** 库名 */
	private String storageRoomName;
	/** 在所在库的别名 */
	private String nameInStroageRoom;

	public String getStorageRoomName() {
		return storageRoomName;
	}

	public void setStorageRoomName(String storageRoomName) {
		this.storageRoomName = storageRoomName;
	}

	public String getNameInStroageRoom() {
		return nameInStroageRoom;
	}

	public void setNameInStroageRoom(String nameInStroageRoom) {
		this.nameInStroageRoom = nameInStroageRoom;
	}

	public String getStorageRoomId() {
		return storageRoomId;
	}

	public void setStorageRoomId(String storageRoomId) {
		this.storageRoomId = storageRoomId;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	public String getTagName() {
		return tagName;
	}

	public void setTagName(String tagName) {
		this.tagName = tagName;
	}

	public double getPv() {
		return pv;
	}

	public void setPv(double pv) {
		this.pv = pv;
	}

}
