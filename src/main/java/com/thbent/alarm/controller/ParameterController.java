package com.thbent.alarm.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.thbent.alarm.model.Parameter;
import com.thbent.alarm.service.ParameterService;
import com.thbent.alarm.utils.Constant;
import com.thbent.alarm.utils.GetRetMapUtil;
import com.thbent.alarm.utils.UUIDUtil;

@Controller
public class ParameterController {

	@Autowired
	private ParameterService parameterService;

	/**
	 * 获取所有参数的中文名称
	 * 
	 * @return
	 */
	@RequestMapping("getAllNameZh")
	@ResponseBody
	public Object getAllNameZh() {
		List<Parameter> nameZhList = parameterService.getAllNameZh();
		return nameZhList;
	}

	/**
	 * 获取所有的报警参数
	 * 
	 * @return
	 */
	@RequestMapping("getAllParameters")
	@ResponseBody
	public Object getAllModule() {
		List<Parameter> paramterList = parameterService.findAllParameters();
		return paramterList;
	}

	/**
	 * 根据名称查询报警参数
	 * 
	 * @param name
	 * @return
	 */
	@RequestMapping("getParameter")
	@ResponseBody
	public Object getParameter(
			@RequestParam(value = "name", required = true) String name) {
		List<Parameter> paramterList = parameterService
				.findParameterByNameZh(name);
		return paramterList;
	}

	/**
	 * 添加手机号
	 * 
	 * @return
	 */
	@RequestMapping("parameter/addPhone")
	@ResponseBody
	public Object addPhone(
			Parameter parameter,
			@RequestParam(value = "name_value", required = true) String nameValue) {

		List<String> storageRoomIdList = JSON.parseArray(nameValue,
				String.class);
		Map<String, Object> retMap = new HashMap<String, Object>();
		parameter.setId(UUIDUtil.getUUID());
		parameter.setName("phone");
		parameter.setIsMultiple(1);
		parameter.setModule("手机号");
		parameter.setNameZh("手机号");
		parameterService.addPhone(parameter, storageRoomIdList);
		retMap.put("retMessage", "添加手机号成功！");
		return retMap;
	}

	/**
	 * 检查手机号是否已经存在
	 * 
	 * @return
	 */
	@RequestMapping("parameter/checkPhone")
	@ResponseBody
	public Object checkPhone(
			@RequestParam(value = "value", required = true) String value) {
		Map<String, Object> retMap = new HashMap<String, Object>();
		Boolean isExist = parameterService.isPhoneExist(value);
		if (isExist) {
			retMap.put("isOk", "ok");
			retMap.put(Constant.RET_MESSAGE, "手机号已存在！");
		}
		return retMap;
	}

	/**
	 * 查询所有添加过得手机号
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping("findAllPhone")
	public String findAllPhone(Model model) {
		List<Parameter> phoneList = parameterService
				.findParametersByName(Constant.PARANMETER_PHONE);
		model.addAttribute("phoneList", phoneList);
		return "addPhone";

	}

	/**
	 * 删除手机号码
	 * 
	 * @param phoneId
	 * @return
	 */
	@RequestMapping("parameter/delete")
	@ResponseBody
	public Object deletePhone(
			@RequestParam(value = "value", required = true) String phoneId) {
		parameterService.deletePhone(phoneId);
		Map<String, Object> retMap = GetRetMapUtil.getRetMap("删除成功！");
		return retMap;
	}

	/**
	 * 跳转到修改手机号页面
	 * 
	 * @param model
	 * @param id
	 * @return
	 */
	@RequestMapping("toUpdatePhone")
	public String toUpdatePhonea(Model model,
			@RequestParam(value = "value", required = true) String id) {
		Parameter parameter = parameterService.findById(id);
		model.addAttribute("parameter", parameter);
		return "toUpdatePhone";
	}

	/**
	 * 修改手机号
	 * 
	 * @return
	 */
	@RequestMapping("updatePhone")
	@ResponseBody
	public Object updatePhone(Parameter parameter) {
		parameterService.updatePhone(parameter);
		Map<String, Object> retMap = GetRetMapUtil.getRetMap("修改成功！");
		return retMap;
	}

}
