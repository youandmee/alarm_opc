package com.thbent.alarm.controller;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.thbent.alarm.model.User;
import com.thbent.alarm.utils.Constant;

/**
 * 负责转发的控制层
 * 
 * @author Administrator
 *
 */
@Controller
public class ForwardController {

	@RequestMapping("toJsp")
	public Object toJsp(
			@RequestParam(value = "jspName", required = true) String jspName,
			HttpSession session) {
		User sessionUser = (User) session.getAttribute(Constant.SESSION_USER);
		if (sessionUser == null) {
			return new ModelAndView("redirect:/login.jsp");
		}
		return jspName;
	}

}
