package com.thbent.alarm.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.thbent.alarm.model.StorageRoom;
import com.thbent.alarm.service.StorageRoomService;

@Controller
public class StorageRoomController {

	@Autowired
	private StorageRoomService storageRoomService;

	@RequestMapping("storageRoom/findAll")
	@ResponseBody
	public Object getAll() {
		List<StorageRoom> storageRoomList = storageRoomService.getAll();
		return storageRoomList;
	};
}
