package com.thbent.alarm.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.thbent.alarm.exception.LoginException;
import com.thbent.alarm.model.User;
import com.thbent.alarm.service.UserService;
import com.thbent.alarm.utils.Constant;

/**
 * 用户控制层
 * 
 * @author Administrator
 *
 */
@Controller
public class UserController {
	private static final Logger LOGGER = LogManager.getLogger(UserController.class);

	@Autowired
	private UserService userService;

	@RequestMapping("login")
	@ResponseBody
	public Object login(@RequestParam(value = "username", required = true) String username,
			@RequestParam(value = "password", required = true) String password, HttpSession session) {

		Map<String, Object> retMap = new HashMap<String, Object>();
		User sessionUser = null;
		try {
			sessionUser = userService.login(username, password);
			session.setAttribute(Constant.SESSION_USER, sessionUser);
			retMap.put("loginMessage", "true");// 2代表用户名和密码正确
		} catch (LoginException e) {
			retMap.put("loginMessage", e.getMessage());
			retMap.put("errorCount", e.getErrorCount());
			LOGGER.error(e.getMessage(), e);
		}
		return retMap;
	}

	/**
	 * 重置密码
	 * 
	 * @return
	 */
	@RequestMapping("reset")
	@ResponseBody
	public Object reset() {
		Map<String, Object> paramMap = new HashMap<>();
		Map<String, Object> retMap = new HashMap<>();
		try {
			paramMap.put("username", "admin");
			paramMap.put("password", "11235813");
			paramMap.put("errorCount", 0);
			userService.modifyPassword(paramMap);
			retMap.put("isOk", "ok");
			retMap.put("retMessage", "重置密码成功，重置密码为11235813");
		} catch (Exception e) {
			retMap.put("isOk", "no");
			retMap.put("retMessage", "重置密码失败，请稍后重试....");
			LOGGER.error(e.getMessage(), e);
		}
		return retMap;
	}

	/**
	 * 修改密码
	 * 
	 * @param newPassword
	 * @param confirmPassword
	 * @param session
	 * @return
	 */
	@RequestMapping("user/changePasword")
	@ResponseBody
	public Object changPassword(@RequestParam(value = "newPassword", required = true) String newPassword,
			HttpSession session) {

		Map<String, Object> retMap = new HashMap<String, Object>();
		User sessionUser = (User) session.getAttribute(Constant.SESSION_USER);
		User user = userService.changePassword(sessionUser.getUsername(), newPassword);
		session.setAttribute(Constant.SESSION_USER, user);
		retMap.put("retMessage", "修改密码成功！");
		return retMap;
	}

	/**
	 * 退出登录
	 * 
	 * @param session
	 * @return
	 */
	@RequestMapping("user/logout")
	public ModelAndView logout(HttpSession session) {
		session.removeAttribute(Constant.SESSION_USER);
		return new ModelAndView("redirect:/login.jsp");
	}
}
