package com.thbent.alarm.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.thbent.alarm.model.PhoneAndProbe;
import com.thbent.alarm.service.PhoneAndProbeService;

@Controller
public class PhoneAndProbeController {

	@Autowired
	private PhoneAndProbeService phoneAndProbeService;

	@RequestMapping("phoneAndProbe/findByPhone")
	@ResponseBody
	public Object findByPhone(@RequestParam(value = "value") String phoneId) {
		List<PhoneAndProbe> phoneAndProbeList = phoneAndProbeService
				.findByPhoneId(phoneId);
		return phoneAndProbeList;
	}
}
