package com.thbent.alarm.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.thbent.alarm.model.AlarmData;
import com.thbent.alarm.service.AlarmDataService;
import com.thbent.alarm.utils.GetRetMapUtil;

@Controller
public class AlarmDataController {

	@Autowired
	private AlarmDataService alarmDataService;

	@RequestMapping("alarmData/getAllAlarmData")
	public String getAllAlarmData(Model model) {
		List<AlarmData> alarmDataList = alarmDataService.findAll();
		model.addAttribute("alarmDataList", alarmDataList);
		return "alarmData";
	}

	@RequestMapping("alarmData/toUpdataAlarmData")
	public String toUpdateAlarmData(Model model,
			@RequestParam(value = "id", required = true) String id) {
		AlarmData alarmData = alarmDataService.findAlarmDataById(id);
		model.addAttribute("alarmData", alarmData);
		return "toUpdateAlarmData";
	}

	@RequestMapping("alarmData/updateAlarmData")
	@ResponseBody
	public Object updateAlarmData(AlarmData alarmData) {
		alarmDataService.updateAlarmData(alarmData);
		Map<String, Object> retMap = GetRetMapUtil.getRetMap("修改成功！");
		return retMap;
	};
}
