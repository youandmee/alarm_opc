package com.thbent.alarm.controller;

/**
 * 设备控制层
 */
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.thbent.alarm.model.Probe;
import com.thbent.alarm.service.ProbeService;

@Controller
public class ProbeController {

	@Autowired
	private ProbeService probeService;

	@RequestMapping("probe/findAll")
	@ResponseBody
	public Object findAll() {
		List<Probe> probeList = probeService.findAll();
		return probeList;
	}
}
