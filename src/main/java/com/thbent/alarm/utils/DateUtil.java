package com.thbent.alarm.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtil {

	private static SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");

	public static String getDate(Date date) {
		return sdf.format(date);
	}

}
