package com.thbent.alarm.utils;

public class TypeConvert {

	public static int integerToInt(Integer value) {
		return value == null ? 0 : value.intValue();
	}

}
