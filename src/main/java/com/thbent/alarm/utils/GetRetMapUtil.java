package com.thbent.alarm.utils;

import java.util.HashMap;
import java.util.Map;

public class GetRetMapUtil {

	public static Map<String, Object> getRetMap(String message) {
		Map<String, Object> retMap = new HashMap<String, Object>();
		retMap.put("retMessage", message);
		return retMap;
	}
}
