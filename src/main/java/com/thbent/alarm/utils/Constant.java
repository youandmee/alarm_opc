package com.thbent.alarm.utils;

public class Constant {

	/** session的用户名 */
	public static final String SESSION_USER = "sessionUser";
	/** 初次报警时间间隔参数名 */
	public static final String PARANMETER_FIRST_ALARM_TIME = "firstAlarmTime";
	/** 重复报警时间间隔参数名 */
	public static final String PARANMETER_REPETITION_TIME = "repetitionTime";
	/** 手机号参数名 */
	public static final String PARANMETER_PHONE = "phone";
	/** ajax返回参数名 */
	public static final String RET_MESSAGE = "retMessage";

}
