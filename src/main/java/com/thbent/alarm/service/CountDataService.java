package com.thbent.alarm.service;

import com.thbent.alarm.model.CountData;

public interface CountDataService {

	void modifyCount(CountData countData);

}
