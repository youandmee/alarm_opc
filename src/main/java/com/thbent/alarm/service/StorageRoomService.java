package com.thbent.alarm.service;

import java.util.List;

import com.thbent.alarm.model.StorageRoom;

public interface StorageRoomService {

	List<StorageRoom> getAll();

}
