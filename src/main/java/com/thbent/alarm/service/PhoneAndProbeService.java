package com.thbent.alarm.service;

import java.util.List;

import com.thbent.alarm.model.PhoneAndProbe;

public interface PhoneAndProbeService {

	List<PhoneAndProbe> findAll();

	List<PhoneAndProbe> findByPhoneId(String phoneId);

}
