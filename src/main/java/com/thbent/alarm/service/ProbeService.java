package com.thbent.alarm.service;

import java.util.List;

import com.thbent.alarm.model.Probe;

public interface ProbeService {

	List<Probe> findAll();

}
