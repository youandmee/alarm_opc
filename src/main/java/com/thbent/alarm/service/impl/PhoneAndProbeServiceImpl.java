package com.thbent.alarm.service.impl;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.thbent.alarm.dao.PhoneAndProbeDao;
import com.thbent.alarm.model.PhoneAndProbe;
import com.thbent.alarm.service.PhoneAndProbeService;

@Service("phoneAndProbeService")
public class PhoneAndProbeServiceImpl implements PhoneAndProbeService {

	private static final Logger LOGGER = LogManager
			.getLogger("phoneAndProbeService");

	@Autowired
	private PhoneAndProbeDao PhoneAndProbeDao;

	@Override
	public List<PhoneAndProbe> findAll() {

		return PhoneAndProbeDao.findAll();
	}

	@Override
	public List<PhoneAndProbe> findByPhoneId(String phoneId) {
		return PhoneAndProbeDao.findByPhoneId(phoneId);
	}

}
