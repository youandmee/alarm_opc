package com.thbent.alarm.service.impl;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thbent.alarm.dao.ParameterDao;
import com.thbent.alarm.dao.PhoneAndProbeDao;
import com.thbent.alarm.dao.ProbeDao;
import com.thbent.alarm.model.Parameter;
import com.thbent.alarm.model.PhoneAndProbe;
import com.thbent.alarm.model.Probe;
import com.thbent.alarm.service.ParameterService;
import com.thbent.alarm.utils.Constant;
import com.thbent.alarm.utils.UUIDUtil;

@Service("parameterService")
public class ParameterServiceImpl implements ParameterService {
	private static final Logger LOGGER = LogManager
			.getLogger("ParameterServiceImpl");

	@Autowired
	private ParameterDao parameterDao;
	@Autowired
	private PhoneAndProbeDao phoneAndProbeDao;
	@Autowired
	private ProbeDao probeDao;

	/**
	 * 获取所有中文名称，去重手机号
	 */
	@Override
	public List<Parameter> getAllNameZh() {
		return parameterDao.findAllNameZh();
	}

	@Override
	public void add(Parameter parameter) {
		parameterDao.add(parameter);
	}

	@Override
	public Parameter find(String name) {
		return parameterDao.find(name);
	}

	@Override
	public List<Parameter> findAllParameters() {
		return parameterDao.findAllParameters();
	}

	@Override
	public List<Parameter> findParameterByName(String name) {
		return parameterDao.findParameterByName(name);
	}

	@Override
	public Parameter findSingleParameter(String name) {
		return parameterDao.findSingleParameter(name);
	}

	/**
	 * 添加手机号
	 */

	@Override
	@Transactional(rollbackFor = { Throwable.class })
	public void addPhone(Parameter parameter, List<String> storageRoomIdList) {
		add(parameter);
		for (String storageRoomId : storageRoomIdList) {
			List<Probe> probeList = probeDao.findByStroraeRoomId(storageRoomId);
			for (Probe probe : probeList) {
				PhoneAndProbe phoneAndProbe = new PhoneAndProbe();
				phoneAndProbe.setId(UUIDUtil.getUUID());
				phoneAndProbe.setPhoneId(parameter.getId());
				phoneAndProbe.setProbeName(probe.getTagName());
				phoneAndProbe.setStorageRoomId(storageRoomId);
				phoneAndProbeDao.add(phoneAndProbe);
			}
		}
	}

	/**
	 * 根据名称查询多个对象
	 */
	@Override
	public List<Parameter> findParametersByName(String name) {
		return parameterDao.findParametersByName(name);
	}

	/**
	 * 根据手机号能查询该手机号是否存在
	 */
	@Override
	public Boolean isPhoneExist(String phoneNumber) {
		Boolean isExist = false;// false代表不存在
		List<Parameter> phoneList = parameterDao
				.findParametersByName(Constant.PARANMETER_PHONE);
		for (Parameter phone : phoneList) {
			if (phoneNumber.equals(phone.getValue())) {
				isExist = true;
				break;
			}
		}
		return isExist;
	}

	/**
	 * 删除电话号码
	 */
	@Override
	@Transactional(rollbackFor = { Throwable.class })
	public void deletePhone(String phoneId) {
		phoneAndProbeDao.deleteByPhoneId(phoneId);
		deleteById(phoneId);
	}

	private void deleteById(String id) {
		parameterDao.deleteById(id);
	}

	/**
	 * 根据id查询
	 */
	@Override
	public Parameter findById(String id) {
		return parameterDao.findById(id);
	}

	/**
	 * 修改手机号
	 */
	@Override
	public void updatePhone(Parameter parameter) {
		parameterDao.updatePhone(parameter);
	}

	/**
	 * 根据中文名称查询参数
	 */
	@Override
	public List<Parameter> findParameterByNameZh(String nameZh) {
		// TODO Auto-generated method stub
		return parameterDao.findParameterByNameZh(nameZh);
	}

}
