package com.thbent.alarm.service.impl;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.thbent.alarm.dao.ProbeDao;
import com.thbent.alarm.model.Probe;
import com.thbent.alarm.service.ProbeService;

@Service("probleService")
public class ProbeServiceImpl implements ProbeService {

	private static final Logger LOGGER = LogManager.getLogger("probleService");
	@Autowired
	private ProbeDao probeDao;

	@Override
	public List<Probe> findAll() {

		return probeDao.findAll();
	}

}
