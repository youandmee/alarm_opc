package com.thbent.alarm.service.impl;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.thbent.alarm.dao.AlarmDataDao;
import com.thbent.alarm.model.AlarmData;
import com.thbent.alarm.service.AlarmDataService;

@Service("alarmDataService")
public class AlarmDataServicceImpl implements AlarmDataService {

	private static final Logger LOGGER = LogManager
			.getLogger("alarmDataService");

	@Autowired
	private AlarmDataDao alarmDataDao;

	/**
	 * 查询所有
	 */
	@Override
	public List<AlarmData> findAll() {
		return alarmDataDao.findAll();
	}

	/**
	 * 根据id查询报警参数
	 */
	@Override
	public AlarmData findAlarmDataById(String id) {
		return alarmDataDao.findAlarmDataById(id);
	}

	/**
	 * 修改
	 */
	@Override
	public void updateAlarmData(AlarmData alarmData) {
		alarmDataDao.updateAlarmData(alarmData);

	}
}
