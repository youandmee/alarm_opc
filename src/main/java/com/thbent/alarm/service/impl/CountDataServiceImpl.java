package com.thbent.alarm.service.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.thbent.alarm.dao.CountDataDao;
import com.thbent.alarm.model.CountData;
import com.thbent.alarm.service.CountDataService;

@Service("countDataService")
public class CountDataServiceImpl implements CountDataService {

	private static Logger LOGGER = LogManager.getLogger("countDataService");

	@Autowired
	private CountDataDao countDataDao;

	@Override
	public void modifyCount(CountData countData) {
		countDataDao.updataCount(countData);
	}

}
