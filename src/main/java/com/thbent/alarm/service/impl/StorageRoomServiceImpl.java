package com.thbent.alarm.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.thbent.alarm.dao.StorageRoomDao;
import com.thbent.alarm.model.StorageRoom;
import com.thbent.alarm.service.StorageRoomService;

@Service("storageRoomServiceImpl")
public class StorageRoomServiceImpl implements StorageRoomService {

	@Autowired
	private StorageRoomDao storageRoomDao;

	@Override
	public List<StorageRoom> getAll() {
		return storageRoomDao.getAll();
	}

}
