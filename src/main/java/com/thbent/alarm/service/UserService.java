package com.thbent.alarm.service;

import java.util.Map;

import com.thbent.alarm.exception.LoginException;
import com.thbent.alarm.model.User;

public interface UserService {

	User findByUserName(String username);

	void modifyErrorCount(String username, Integer errorCount);

	void modifyPassword(Map<String, Object> paramMap);

	User login(String username, String password) throws LoginException;

	User changePassword(String username, String newPassword);

}
