package com.thbent.alarm.service;

import java.util.List;

import com.thbent.alarm.model.AlarmData;

public interface AlarmDataService {

	List<AlarmData> findAll();

	AlarmData findAlarmDataById(String id);

	void updateAlarmData(AlarmData alarmData);

}
