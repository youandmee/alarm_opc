package com.thbent.alarm.service;

import java.util.List;

import com.thbent.alarm.model.Parameter;

public interface ParameterService {

	void add(Parameter parameter);

	Parameter find(String name);

	List<Parameter> findAllParameters();

	List<Parameter> findParameterByName(String name);

	Parameter findSingleParameter(String name);

	void addPhone(Parameter parameter, List<String> probeNameList);

	List<Parameter> findParametersByName(String string);

	Boolean isPhoneExist(String value);

	void deletePhone(String phoneId);

	Parameter findById(String id);

	void updatePhone(Parameter parameter);

	List<Parameter> findParameterByNameZh(String nameZh);

	List<Parameter> getAllNameZh();

}
