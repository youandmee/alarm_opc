package com.thbent.alarm.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.thbent.alarm.model.User;
import com.thbent.alarm.utils.Constant;

public class LoginInterceptor implements HandlerInterceptor {

	@Override
	public boolean preHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler) throws Exception {
		User sessionUser = (User) request.getSession().getAttribute(
				Constant.SESSION_USER);
		if (sessionUser == null) {
			String contextPath = request.getContextPath();
			String path = request.getRequestURI();
			if (path.contains("reset")) {
				return true;
			}
			response.sendRedirect(contextPath + "/login.jsp");
			return false;
		}
		return true;
	}

	@Override
	public void postHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {

	}

	@Override
	public void afterCompletion(HttpServletRequest request,
			HttpServletResponse response, Object handler, Exception ex)
			throws Exception {

	}

}
