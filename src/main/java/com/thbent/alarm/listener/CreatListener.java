package com.thbent.alarm.listener;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.thbent.alarm.port.ReadSerialPort;

public class CreatListener implements ServletContextListener {

	private static final Logger LOGGER = LogManager.getLogger(CreatListener.class);

	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void contextInitialized(ServletContextEvent event) {

		ReadSerialPort readSerialPort = ReadSerialPort.get();
		try {
			readSerialPort.startComPort();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			LOGGER.error(e.getMessage(), e);
		}
	}

}
