<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page isELIgnored="false"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>登录页面</title>
<script type="text/javascript" src="static/js/jquery.js"></script>
<script type="text/javascript">
	$(function(){
		//隐藏重置密码的div
		$("#pwdDiv").hide();
		$("#login").click(function(){
			var username = $("#username").val();
			var password = $("#password").val();
			//验证用户名是否为空
			if(username==''){
				$("#loginMessage").html("<font color='red'>用户名不能为空!</font>");
				return;
			}
			if(password==''){
				$("#loginMessage").html("<font color='red'>密码不能为空!</font>");
				return;
			}
			
			$.ajax({
				url:"${pageContext.request.contextPath}/login",
				type:'post',
				data:{
					username:username,
					password:password
				},
				success:function(data){
					if(data.loginMessage == 'true'){
						$("#loginMessage").html("<font color='green'>正在登录请稍后...</font>");
						window.location.href = "${pageContext.request.contextPath}/toJsp?jspName=index";
					}else{
						$("#loginMessage").html("<font color='red'>"+data.loginMessage+"</font>");
						if(data.errorCount >= 5){
							$("#pwdDiv").show();
						}
					}
				}
			});
		});
		
		//给重置按钮添加单击事件
		$("#resetBtn").click(function(){
			$.ajax({
				url:"${pageContext.request.contextPath}/reset",
				type:'post',
				success:function(data){
					if(data.isOk == 'ok'){
						htmlStr = "<font color='#FFFFCC'>"+data.retMessage+"</font>";
						$("#loginMessage").html(htmlStr);
						$("#pwdDiv").hide();
					}else{
						htmlStr = "<font color='red'>"+data.retMessage+"</font>";
						$("#loginMessage").html(htmlStr);
					}
				}
			});
		});
		
	});
</script>
</head>
<body>
	<r>
	<br>
	<br>
	<h3 align="center">温湿度检测报警系统</h3>
	<table align="center" >
		<tr>
			<td>用户名：</td>
			<td><input id="username" type="text" name="username"></input></td>
		</tr>
		<tr>
			<td>密码：</td>
			<td><input id="password" type="password" name="password"/><br></td>
		</tr>
		<tr align="center">
			<td colspan="1"><input id="login"  type="button" value="用户登录" /></td>
		</tr>
	</table>
	<div id="pwdDiv" align="center">
		<button id="resetBtn">重置密码</button>
	</div>
	<div id="loginMessage" align="center"></div>
</body>
</html>