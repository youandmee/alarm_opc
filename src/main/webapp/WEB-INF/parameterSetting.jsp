<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page isELIgnored="false"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>参数设置页面</title>
<script type="text/javascript" src="static/js/jquery.js"></script>
<script type="text/javascript">
	$(function(){
		//页面加载完成执行下面的ajax
		$.ajax({
			url:"${pageContext.request.contextPath}/getAllNameZh",
			type:"post",
			success:function(data){
				$("#mySelect").html();
				var htmlStr = "";
				$(data).each(function(){
					htmlStr += "<option value='"+this.nameZh+"'>"+this.nameZh+"</option>";
				});
				$("#mySelect").html(htmlStr);
			}
			
		});
		
		$("#findBtn").click(function(){
			var name = $("#mySelect option:selected").val();
			$.ajax({
				url:"getParameter",
				type:'post',
				data:{
					name:name
				},
				success:function(data){
					$("#parameterBody").html("");
					var html = "";
					$(data).each(function(){
						html += "<tr>";
						html += "<td>"+this.nameZh+"</td>";
						html += "<td>"+this.value+"</td>";
						if(this.notes==null){
							html += "<td>无</td>";
						}else{
							html += "<td>"+this.notes+"</td>";
						}
						if(this.isMultiple==0){
							html += "<td><button value='"+this.id+"' onclick='updateParameter(this.value)'>修改</button></td>";
						}else{
							html += "<td><button value='"+this.id+"' onclick='updateParameter(this.value)'>修改</button>&nbsp;<button value='"+this.id+"' onclick='updateParameter(this.value)'>删除</button></td>";
						}
						html += "</tr>";
					});
					$("#parameterBody").html(html);
				}
			});
		});
		
		//查询所有的报警参数
		$("#findAllBtn").click(function(){
			$.ajax({
				url:"${pageContext.request.contextPath}/getAllParameters",
				type:'post',
				success:function(data){
					$("#parameterBody").html("");
					var html = "";
					$(data).each(function(){
						html += "<tr>";
						html += "<td align='center'>"+this.nameZh+"</td>";
						html += "<td align='center'>"+this.value+"</td>";
						if(this.notes==null){
							html += "<td align='center'>无</td>";
						}else{
							html += "<td align='center'>"+this.notes+"</td>";
						}
						if(this.isMultiple==0){
								html += "<td align='center'><button value='"+this.id+"' onclick='updateParameter(this.value)'>修改</button></td>";
						}else{
								html += "<td align='center'><button value='"+this.id+"' onclick='updateParameter(this.value)'>修改</button>&nbsp;<button value='"+this.id+"' onclick='deleteParameter(this.value)'>删除</button></td>";
						}
						html += "</tr>";
					});
					$("#parameterBody").html(html);
				},
				error:function(){
					alert("网络错误，请稍后再试....");
				}
			});
		}); 
		
		$("#returnBtn").click(function(){
			window.location.href="${pageContext.request.contextPath}/toJsp?jspName=index";
		});
	});
	
	function deleteParameter(value){
		 if (confirm("确认要删除？")) {
			 $.ajax({
					url:"${pageContext.request.contextPath}/parameter/delete",
					type:"post",
					data:{
						value:value
					},
					success:function(data){
						alert(data.retMessage);
						location.reload();
					},
					error:function(){
					}
				});
	        }
	}
	
	function updateParameter(value){
		window.location.href="${pageContext.request.contextPath}/toUpdatePhone?value="+value;
	}
	
	/* function updateParameter(id){
		window.location.href="${pageContext.request.contextPath}/toUpdateParameter?value="+id;
	} */
</script>
</head>
<body>
	名称：<select id="mySelect">
		</select>
		<button id="findBtn">查询</button>&nbsp;<button id="findAllBtn">查询所有</button>
		<button id="returnBtn">返回首页</button><br>
		<br>
		<table border="1" >
			<thead>
				<tr>
					<td align="center">名称</td>
					<td align="center">值</td>
					<td align="center">备注</td>
					<td align="center">操作</td>
				</tr>
			</thead>
			<tbody id="parameterBody">
			</tbody>
		</table>
</body>
</html>