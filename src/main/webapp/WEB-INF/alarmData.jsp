<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page isELIgnored="false"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>报警参数设置</title>
<script type="text/javascript" src="${pageContext.request.contextPath}/static/js/jquery.js"></script>
<script type="text/javascript">
	$(function(){
		$("#returnBtn").click(function(){
			window.location.href="${pageContext.request.contextPath}/toJsp?jspName=index";
		});
	});
	function updateAlarmData(id){
		window.location.href="${pageContext.request.contextPath}/alarmData/toUpdataAlarmData?id="+id;
	}
</script>
</head>
<body>
	<table align="center" border="1" cellpadding="1">
		<thead>
			<tr>
				<td><button id="returnBtn">返回首页</button></td>
			</tr>
			<tr>
				<td align="center">仓库名称</td>
				<td align="center">参数上限</td>
				<td align="center">参数下限</td>
				<td align="center">操作</td>
			</tr>
		</thead>
		<tbody>
			<c:if test="${not empty alarmDataList}">
				<c:forEach items="${alarmDataList}" var="alarmData" >
					<tr>
						<td align="center">${alarmData.storageRoomName}</td>
						<td align="center">${alarmData.minValue}℃</td>
						<td align="center">${alarmData.maxValue}℃</td>
						<td align="center"><button value="${alarmData.id}" onclick="updateAlarmData(this.value)">修改</button></td>
					</tr>
				</c:forEach>
			</c:if>
		</tbody>
	</table>
</body>
</html>