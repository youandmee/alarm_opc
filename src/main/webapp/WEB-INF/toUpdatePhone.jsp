<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page isELIgnored="false"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>修改手机号页面</title>
<script type="text/javascript" src="static/js/jquery.js"></script>
<script type="text/javascript">
	$(function(){
		
		$("#updatePhoneBtn").click(function(){
			var value = $("#phone").val();
			var notes = $("#notes").val();
			var id = $("#id").val();
			var name = "${parameter.name}";
			if(name == 'phone'){
				var phoneReg = /^1\d{10}$/; 
				if(!phoneReg.test(value)){ 
				    alert('手机号格式不正确，请输入正确的手机号！'); 
				    return; 
				}else if(checkPhone(value)){
					 alert('手机号已存在！'); 
						return;
				}
			}else{
				 var reg = /^[1-9]\d*$/;
				 if(!reg.test(value)){
					 alert("只能输入正整数值");
					 return;
				 }
			}
			
			$.ajax({
				url:"${pageContext.request.contextPath}/updatePhone",
				type:'post',
				data:{
					value:value,
					notes:notes,
					id:id
				},
				success:function(data){
					alert(data.retMessage);
					if(name == 'phone'){
						window.location.href="${pageContext.request.contextPath}/findAllPhone";
					}else{
						window.location.href="${pageContext.request.contextPath}/toJsp?jspName=parameterSetting";
						
					}
				},
				error:function(){
					alert("网络错误，请稍后再试...");
				}
			});
		});
		
		//失去焦点事件：添加手机号时查询该手机号是否存在
		$("#phone").blur(function(){
			var value = $("#phone").val(); 
			var name = "${name}";
			if(name == 'phone'){
				checkPhone(value);
			}
		});
		
		$("#returnBtn").click(function(){
			window.location.href="${pageContext.request.contextPath}/toJsp?jspName=index";
		});
	});
	
	function checkPhone(value){
		var isExist = true;
		$.ajax({
			url:"${pageContext.request.contextPath}/parameter/checkPhone",
			type:'post',
			async:false,
			data:{
				value:value
			},
			success:function(data){
				if(data.isOk=="ok" ){
					$("#checkPhone").html("<font color='red'>"+data.retMessage+"</font>");
				}else{
					isExist = false;
					$("#checkPhone").html("");
				}
			},
			error:function(){
				alert("网络错误，请稍后重试...");
			}
		});
		return isExist;
	}
</script>
</head>
<body>
	<br>
	${parameter.nameZh}：<input type="text" value="${parameter.value}" id="phone" name="phone"><br>
	备注：<input type="text" value="${parameter.notes}" id="notes" name="notes"><br>
	<input type="hidden" value="${parameter.id}" id="id" name="id"><br>
	<button id="updatePhoneBtn">修改</button>&nbsp;&nbsp;<button id="returnBtn">返回首页</button>
</body>
</html>