<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page isELIgnored="false"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>修改密码页面</title>
<script type="text/javascript" src="static/js/jquery.js"></script>
<script type="text/javascript">
	$(function(){
		$("#changePasswordBtn").click(function(){
			var newPassword = $("#newPassword").val();
			var confirmPassword = $("#confirmPassword").val();
			
			if(newPassword == ""){
				$("#myDiv").html("<font color='red'>新密码不能为空！</font>");
				return;
			}else if(confirmPassword == ""){
				$("#myDiv").html("<font color='red'>确认密码不能为空！</font>");
				return;
			}else if(newPassword != confirmPassword){
				$("#myDiv").html("<font color='red'>两次输入的密码不一致！</font>");
				return;
			}else if(newPassword.length > 10){
				$("#myDiv").html("<font color='red'>密码长度不能超过10位！</font>");
				return;
			}else{
				$.ajax({
					url:"${pageContext.request.contextPath}/user/changePasword",
					type:'post',
					data:{
						newPassword:newPassword,
						confirmPassword:confirmPassword
					},
					success:function(data){
							$("#myDiv").html("<font color='green'>"+data.retMessage+"</font>");
					},
					error:function(){
						$("#myDiv").html("<font color='green'>网络错误，请稍后重试</font>");
					}
				});
			}
		});
		
		$("#returnIndexBtn").click(function(){
			window.location.href="${pageContext.request.contextPath}/toJsp?jspName=index";
		});
	});
</script>
</head>
<body>
	请输入新密码：<input type="password" id="newPassword"  name="newPassword"><br>
	请确认新密码：<input type="password" id="confirmPassword"  name="confirmPassword"><br>
	<button id="changePasswordBtn">提交</button>&nbsp;<button id="returnIndexBtn">返回首页</button><br>
	<div id="myDiv"></div>
</body>
</html>