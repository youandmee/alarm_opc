<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page isELIgnored="false"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>首页</title>
<style type="text/css">
	.box {
		position:absolute;
		top:20%; 
		left:50%;  
		margin:-50px 0 0 -100px;  
		width:200px; height:100px; 
		}
</style>
<script type="text/javascript" src="${pageContext.request.contextPath}/static/js/jquery.js"></script>
<script type="text/javascript">
	$(function(){
		$("#addPhoneBtn").click(function(){
			window.location.href="${pageContext.request.contextPath}/findAllPhone";
		});
		$("#addDataBtn").click(function(){
			window.location.href="${pageContext.request.contextPath}/toJsp?jspName=parameterSetting";
		});
		$("#modifyPasswordBtn").click(function(){
			window.location.href="${pageContext.request.contextPath}/toJsp?jspName=modifyPassword";
		});
		$("#alarmDataBtn").click(function(){
			window.location.href="${pageContext.request.contextPath}/alarmData/getAllAlarmData";
		});
		
		$("#logoutBtn").click(function(){
			window.location.href="${pageContext.request.contextPath}/user/logout";
		});
	});
</script>
</head>
<body>
	<div class="box" align="center">
		<button id="addPhoneBtn" style="width: 155px">添加电话号码</button><br>
		<button id="addDataBtn">参数设置</button>
		<button id="modifyPasswordBtn">修改密码</button><br>
		<button id="alarmDataBtn" style="width: 155px">报警参数设置</button>
		<button id="logoutBtn" style="width: 155px">退出</button>
	</div>
</body>
</html>