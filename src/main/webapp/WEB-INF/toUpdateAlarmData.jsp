<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page isELIgnored="false"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>修改报警参数页面</title>
<script type="text/javascript" src="${pageContext.request.contextPath}/static/js/jquery.js"></script>
<script type="text/javascript">
	$(function(){
		
		$("#updateAlarmData").click(function(){
			var id = $("#id").val();
			var minValue = $("#minValue").val();
			var maxValue = $("#maxValue").val();
			var myReg = /^(0|[+-]?((0|([1-9]\d*))\.\d+)?)$/;
			if(!myReg.test(minValue) || !myReg.test(maxValue) ){
				alert("上限值和下限值只能为数字类型！");
				return;
			} if(parseFloat(minValue) >= parseFloat(maxValue)){
				alert("下限值不能大于上限值！");
				return;
			}
			$.ajax({
				url:"${pageContext.request.contextPath}/alarmData/updateAlarmData",
				type:'post',
				data:{
					id:id,
					minValue:minValue,
					maxValue:maxValue
				},
				success:function(data){
					alert(data.retMessage);
					window.location.href="${pageContext.request.contextPath}/alarmData/getAllAlarmData";
				},
				error:function(data){
					alert("网络错误，请稍后再试....");
				}
			});
		});
		
		$("#returnBtn").click(function(){
			window.location.href="${pageContext.request.contextPath}/toJsp?jspName=index";
		});
	});

</script>
</head>
<body>
	仓库名称：<span>${alarmData.storageRoomId}</span><br>
	下限值：<input type="text" id="minValue" value="${alarmData.minValue}" name="minValue"><br>
	上限值：<input type="text" id="maxValue" value="${alarmData.maxValue}" name="maxValue">
	<input type="hidden"id="id" value="${alarmData.id}" name="id">
	<button id="updateAlarmData">修改</button>&nbsp;&nbsp;
	<button id="returnBtn">返回首页</button>
	
</body>
</html>