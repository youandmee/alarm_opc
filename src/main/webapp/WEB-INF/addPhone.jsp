<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page isELIgnored="false"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="UTF-8">
<title>添加手机号页面</title>
<script type="text/javascript" src="static/js/jquery.js"></script>
<script type="text/javascript">
	$(function(){
		
		//页面加载完成之后，首先查出所有库的名字
		$.ajax({
			url:"${pageContext.request.contextPath}/storageRoom/findAll",
			type:'post',
			success:function(data){
				$("#checkboxDiv").html("");
				var htmlStr = "";
				var index = 0;
				$(data).each(function(){
					index ++;
					if(index % 5 == 0){
						htmlStr += "<input type='checkbox' name='storageroom' value='"+this.id+"'/> "+this.storageRoomName+"<br>";
					}else{
						htmlStr += "<input type='checkbox' name='storageroom' value='"+this.id+"'/> "+this.storageRoomName+"&nbsp;";
					}
				});
				$("#checkboxDiv").html(htmlStr);
				
			}
		});
		
		$("#addButton").click(function(){
			var value = $("#phone").val();
			var notes = $("#notes").val();
			//var myreg = /^(((13[0-9]{1})|(15[0-9]{1})|(18[0-9]{1}))+\d{8})$/; 
			var myreg = /^1\d{10}$/; 
			var name_value =[];//定义一个数组        
			$('input[name="storageroom"]:checked').each(function(){
				//遍历每一个名字为storageroom的复选框，其中选中的执行函数       
				name_value.push($(this).val());//将选中的值添加到数组name_value中       
				}); 
			if(!myreg.test(value)){ 
			    alert('手机号格式不正确，请输入正确的手机号！'); 
			    return; 
			}else if(name_value.length == 0){
				 alert('请选择要关联的设备名称！'); 
				    return; 
			}else if(checkPhone(value)){
				 alert('手机号已存在！'); 
				return;
			}
			$.ajax({
				url:"${pageContext.request.contextPath}/parameter/addPhone",
				type:'post',
				data:{
					value:value,
					notes:notes,
					name_value:JSON.stringify(name_value)
				},
				success:function(data){
					alert(data.retMessage);
					location.reload();
				}
			});
		});
		
		//失去焦点事件：添加手机号时查询该手机号是否存在
		$("#phone").blur(function(){
			var value = $("#phone").val(); 
			checkPhone(value);
		});
		
		$("#returnBtn").click(function(){
			window.location.href="${pageContext.request.contextPath}/toJsp?jspName=index";
		});
		
	});
	
	function viewDetails(value){
		$.ajax({
			url:"${pageContext.request.contextPath}/phoneAndProbe/findByPhone",
			type:'post',
			data:{
				value:value
			},
			success:function(data){
				var htmlDivStr = "该手机号所属设仓库为：";
				var index = 0;
				$(data).each(function(){
					index ++;
					htmlDivStr += this.storageRoomName;
					htmlDivStr += "&nbsp;&nbsp;";
					if(index % 25 == 0){
						htmlDivStr +="<br>";
					}
				});
				$("#probeNameDiv").html(htmlDivStr);
			},
			error:function(){
				alert("网络错误，请稍后再试...");
			}
			
		});
	}
	
	function checkPhone(value){
		var isExist = true;
		$.ajax({
			url:"${pageContext.request.contextPath}/parameter/checkPhone",
			type:'post',
			async:false,
			data:{
				value:value
			},
			success:function(data){
				if(data.isOk=="ok" ){
					$("#checkPhone").html("<font color='red'>"+data.retMessage+"</font>");
				}else{
					isExist = false;
					$("#checkPhone").html("");
				}
			},
			error:function(){
				alert("网络错误，请稍后重试...");
			}
		});
		return isExist;
	}
	
	function deletePhone(value){
		 if (confirm("确认要删除？")) {
			 $.ajax({
					url:"${pageContext.request.contextPath}/parameter/delete",
					type:"post",
					data:{
						value:value
					},
					success:function(data){
						alert(data.retMessage);
						location.reload();
					},
					error:function(){
					}
				});
	        }
	}
	
	function updatePhone(value){
		window.location.href="${pageContext.request.contextPath}/toUpdatePhone?value="+value;
	}
	
</script>
</head>
<body>
	请输入手机号：<input type="text" id="phone" name="phone"><span id="checkPhone"></span><br><br>
	备注：<input type="text" id="notes" name="notes"><br><br>
	<div id="checkboxDiv">
		<input type="checkbox" name="vehicle" value="Car" checked="checked" /> I have a car
	</div><br>
	<button id="addButton">添加</button>&nbsp;&nbsp;<button id="returnBtn">返回首页</button><br><br>
	<table border="1">
		<thead>
			<tr>
				<td>编号</td>
				<td>手机号</td>
				<td>备注</td>
				<td>所属设备</td>
				<td>操作</td>
			</tr>
		</thead>
		<tbody>
			<c:choose>
				<c:when test="${not empty phoneList }">
					<c:forEach items="${phoneList}" var="phone" varStatus="s">
						<tr>
							<td>${s.index + 1}</td>
							<td>${phone.value}</td>
							<td>${phone.notes}</td>
							<td><button value="${phone.id}" onclick="viewDetails(this.value)">查看详情</button></td>
							<td>
								<button value="${phone.id}" onclick="updatePhone(this.value)">修改</button>&nbsp;&nbsp;
								<button value="${phone.id}" onclick="deletePhone(this.value)">删除</button>
							</td>
						</tr>
					</c:forEach>
				</c:when>
				<c:otherwise>
					<tr>
						<td colspan="5">还没有添加过手机号</td>
					</tr>
				</c:otherwise>
			</c:choose>
		</tbody>
	</table>
	<div id="probeNameDiv" >
	</div>
</body>
</html>